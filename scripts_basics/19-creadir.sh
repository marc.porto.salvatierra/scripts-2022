#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# prog noudir...
# Err: mkdir no genera sortida
# Programa retorna 0 si tots dirs creats ok
# retorna 1 si error en num arg
# retorna 2 si dir no s'ha creat
# -------------------------------------------------------  


ERR_NARG=1
ERR_MKDIR=2
# 1) Validar arguments

if ! [ $# -ge 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 noudir..."
        exit $ERR_NARG
fi


for noudir in $*
do
	mkdir -p $noudir 2> /dev/null
	if ! [ $? -eq 0 ]; then
		echo "Error: directori $1 no creat"
		status=$ERR_MKDIR
	fi
done
echo "$status"
exit 0

