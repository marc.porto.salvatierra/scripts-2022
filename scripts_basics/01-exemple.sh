#! /bin/bash

# @MarcPorto ASIX-m01
# Febrer 2022

# exemple de primer programa
# Normes:
#	shebang (#!)
#	capçalera: descripcio, data, autor
# ----------------------------------------

echo "Hello World"

nom='pere pou prat'
edat=25

echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'

uname -a
uptime

# echo shlvl mostrara un dos ja que entrem a un altre bash quan l'executem
echo $SHLVL 
echo $SHELL
echo $((4*32))
echo $((edat*3))

read data1 data2
echo -e "$data1\n $data2"

exit 0
