#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Exemples de bucle for
# -----------------------------------


# Mostrar logins numerats
login=$(cut -d: -f1  /etc/passwd)
comptador=0
for elem in $login
do
	((comptador++))
	echo "$comptador: $elem"
done
exit 0

# Mostrar numerats els fitxers del dir actiu
comptador=0
llistat=$(ls)
for elem in $llistat
do
	((comptador++))
	echo "$comptador: $elem"
done
exit 0

# Mostrar arg i numerar
comptador=0 
for arg in $*
do
	comptador=$(($comptador + 1))
	echo "$comptador: $arg"
done
exit 0

# Iterar per la llista d'arguments
for arg in $@   # $@ expandeix els words amb o sense cometes
do
	echo "$arg"
done
exit 0

# Iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
	echo "$nom"
done
exit 0


# Iterar per un conjunt d'elements
for nom in "pere" "pau" "anna" "marta"
do
	echo "$nom"
done
exit 0



