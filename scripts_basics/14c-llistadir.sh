#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Per cada element del dir, dir si es file link o dir
# -------------------------------------------------------

ERR_NARG=1
ERR_DIR=2

# 1) Validar arguments

if [ $# -ne 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_NARG
fi

# 2) Si no es dir
dir=$1
if ! [ -d $dir ];
then	
	echo "Error: directori no trobat"
	echo "L'argument rebut ha de ser un directori vàlid"
	exit $ERR_DIR
fi

# 3) Llistar, per cada element dir si es file link o dir
comptador=1
llista=$(ls $dir)
for elem in $llista
do
	if [ -L "$dir/$elem" ]; then
		echo "$elem es un link"
	elif [ -d "$dir/$elem" ]; then
		echo "$elem es un dir"
	elif [ -f "$dir/$elem" ]; then
		echo "$elem es un file"
	else
		echo "$elem es un altra cosa"
	fi
	 
done
exit 0
