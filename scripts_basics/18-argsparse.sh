#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# prog -a file -b -c -d num -e arg...
# opcions: -a -b -c -d -e
# arguments: pere ....
# file: file
# num: num
# -------------------------------------------------------

ERR_NARG=1
ERR_DIR=2


opcions=""
argument=""
file=""
num=""
# 1) Validar arguments
if ! [ $# -ge 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_NARG
fi

while [ -n "$1" ]
do
	case $1 in 
		"-b"|"-c"|"-e")
			opcions="$opcions $1"
			;;
		"-a")
			opcions="$opcions $1"
			file="$file $2"
			shift
			;;
		"-d")
			opcions="$opcions $1"
			num="$num $2"
			shift
			;;
		*)
			arguments="$arguments $1"

	esac
	shift
done

echo "opcions: $opcions"
echo "arguments: $arguments"
echo "file: $file"
echo "num: $num"
exit 0
