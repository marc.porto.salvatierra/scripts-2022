#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Validar 2 arg
# primer arg es un file
# segon arg es un dir
# prog file... dir
# -------------------------------------------------------


ERR_NARG=1
ERR_FILE=2
ERR_DIR=3


# Validar 2 o mes args
if ! [ $# -ge 2 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 file dir"
        exit $ERR_NARG
fi
llista_files=$(echo "$*" | cut -d' ' -f2-$(($#-1)))
dir=$(echo "$*" | cut -d' ' -f$#)
# llista_files=$(echo "$*" | sed  's/ [^ ]*$//')
# dir=$(echo "$*" | sed 's/^.* //')
echo "$llista_files"
exit 0
# Si un dir
if ! [ -d $dir ];then
	echo "Error: $dir no valid"
	echo "Usage: $0 file dir"
	exit $ERR_DIR

fi

for file in $llista_files
do
	if ! [ -f $file ];then
		echo "Error: $file no valid" >> /dev/stderr
		echo "Usage: $0 file... dir" >> /dev/stderr

	else 
		cp $file $dir
	fi
done
exit 0
