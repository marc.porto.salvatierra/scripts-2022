#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# exemple case
# -----------------------------------

# Exemple
case $1 in
	"dissabte"|"diumenge")
		echo "$1 es festiu";;
	*)
		echo "$1 es laborable"
esac
exit 0

# Exemple 
case $1 in
	[aeiou])
		echo "$1 és una vocal";;
	[bcdfghjklmnpqrstvwxyz])
		echo "$1 es consonant";;
	*)
		echo "$1 es un altra cosa"

esac
exit 0

# Exemple 
case $1 in
	"pere"|"pau"|"joan")
		echo "és nen"
		;;
	"marta"|"anna"|"julia")
		echo "és nena"
		;;
	*)
		echo "es indefinit"
		;;
esac
exit 0
