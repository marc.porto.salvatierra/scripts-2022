#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Rep un mes, diu dies
# prog mes
# -----------------------------------

ERR_NARG=1
ERR_MES=2

# 1) Validar arguments

if [ $# -ne 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 mes"
        exit $ERR_NARG
fi

# 2) Validar rang
if ! [ $1 -ge 1 -a $1 -le 12 ];
then
	echo "Error: nº del mes inexistent"
	echo "Rang de mes: [1-12]"
	exit $ERR_MES
fi

# 3) Dir quants dies
mes=$1
case "$mes" in
	"2")
		echo "El mes $mes te 28 dies"
		;;
	"4"|"6"|"9"|"11")
		echo "El mes $mes te 30 dies"
		;;
	*)
		echo "El mes $mes te 31 dies"
esac
exit 0
