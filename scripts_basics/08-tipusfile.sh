#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# prog dir
# -----------------------------------
ERR_NARG=1
ERR_FILE=2
ERR_NOEXIST=3

# 1) Validar arguments

if [ $# -ne 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 file|dir|link"
        exit $ERR_NARG
fi

# 2) Si es file, dir, link
fit=$1

if ! [ -e $fit ];
then
	echo "$fit no existeix"
	exit $ERR_NOEXIST

elif [ -f $fit ];
then
	echo "$fit es un regular file"
elif [ -L $fit ];
then
	echo "$fit es un link"

elif [ -d $fit ];
then
	echo "$fit es un directori"	

else
	echo "$fit es un altra cosa"

fi
exit 0


