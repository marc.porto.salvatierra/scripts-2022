#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Exercicis d'scripts basics 2
# -------------------------------------------------------



# 12) Programa -h uid…
#    Per a cada uid mostra la informació de l’usuari en format:
#    logid(uid) gname home shell

opcio=$1
shift
for uid in $*
do
	login=$(grep "^[^:].*:[^:].*:$uid:" /etc/passwd | cut -d: -f1)
	gid=$(grep "^[^:].*:[^:].*:$uid:[^:].*:" /etc/passwd | cut -d: -f4) 
	gname=$(echo )
	home=
	shell=
	echo "$login($uid) $gname $home $shell"
done
exit  0

# 11) Idem però rep els GIDs com a arguments de la línia d’ordres.

for gid in $*
do
	gname=$(grep "^[^:].*:[^:].*:$gid:" /etc/group | cut -d: -f1)
	users=$(grep "^[^:].*:[^:].*:$gid:" /etc/group | cut -d: -f4)
	echo "gname: $gname, gid: $gid, users: $users"

done
exit 0

# 10) Programa: prog.sh 
# 	Rep per stdin GIDs i llista per stdout la informació de cada un 
# d’aquests grups, en format: gname: GNAME, gid: GID, users: USERS

gname=""
gid=""
users=""

while read -r line
do
	gname=$(grep "^[^:].*:[^:].*:$line:" /etc/group | cut -d: -f1)
	users=$(grep "^[^:].*:[^:].*:$line:" /etc/group | cut -d: -f4)
	echo "gname: $gname, gid: $line, users: $users"
done
exit 0
	

# 9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#   Escriure el codi que desa en les variables: opcions, cognom, edat i 
# arguments els valors corresponents.
#   No cal validar ni mostrar res!
#   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#   retona:     opcions «-r -j», cognom «puig», edat «18», arguments 
# «wheel postgres ldap»

opcions=""
cognom=""
edat=""
arguments=""

while [ "$1" ]
do
	case $1 in
		"-r"|"-m"|"-j")
			opcions="$opcions $1"
			;;
		"-c")
			opcions="$opcions $1"
			cognom="$cognom $2"
			shift
			;;
		"-e")
			opcions="$opcions $1"
			edat="$edat $2"
			shift
			;;
		*)
			arguments="$arguments $1"
			;;	
	esac
	shift
done
echo "opcions: $opcions"
echo "arguments: $arguments"
echo "cognom: $cognom"
echo "edat: $edat"
exit 0

# 8) Programa: prog file…
#   a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar
# per stdout el nom del file comprimit si s’ha comprimit correctament, o un 
# missatge d’error per stderror si no s’ha pogut comprimir. En finalitzar es 
# mostra per stdout quants files ha comprimit. 
#     Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#   b) Ampliar amb el cas: prog -h|--help.
ERR_NARGS=1
ERR_COMPR=2
comptador=0
if ! [ $# -ge 2 ]; then
	echo "Error: nº d'arguments incorrecte"
	echo "Usage $0 file..."
	exit $ERR_NARGS
fi

for file in $*
do
	if [ -f $file ]; then
		gzip $file &> /dev/null	
		if [ $? -eq 0 ]; then
			echo "$file comprimit correctament"
			((comptador++))
		else
			echo "Error: $file no comprimit"
			status=2
		fi
	else
		echo "Error: $file no valid"
		status=2
	fi	
	
done
echo "nº de files comprimits: $comptador"
exit $status


# 7) Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el 
# flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es 
# crida amb -d valida que tots quatre són directoris.
#    Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis. 
#    Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2. 
# b) Ampliar amb el cas: prog -h|--help.


ERR_NARGS=1
ERR_OPT=3

if ! [ $# -eq 5 ]; then
	echo "Error: nº d'arguments incorrecte"
	echo "Usage: -f|-d arg1 arg2 arg3 arg4"
	exit $ERR_NARGS
fi

if [ "$1" != "-f" -o "$1" != "-d"  ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_OPT
fi

opcio=$1
shift

for arg in $*
do
	if ! [ $opcio $arg ]; then
		status=2	
	fi
done
exit $status


# 6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
while read -r line
do
	echo "$line" | sed -r 's/(^[A-Z]).* /\1. /'
done
exit 0

# 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
while read -r line
do
	chars=$(echo $line | wc -m)
	if [ $chars -lt 50 ]; then
	       echo "$line"	
	fi
done
exit 0

# 4) Processar stdin mostrant per stdout les línies numerades i en majúscules..
comptador=1
while read -r line
do
	mayus=$(echo $line | tr '[:lower:]' '[:upper:]')
	echo "$comptador: $mayus"
	((comptador++))
done
exit 0
# 3) Processar arguments que són matricules: 
# a) llistar les vàlides, del tipus: 9999-AAA. 
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)
status=0
for mat in $*
do
	echo $mat | egrep  "^[0-9]{4}-[A-Z]{3}$" 2> /dev/null
		if ! [ $? -eq 0 ];then
		echo "Error: $mat matricula no valida" > /dev/stderr
		((status++))
	fi
done
exit $status
# 2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
comptador=0
for arg in $*
do
	 echo $arg | egrep '.{3,}' &> /dev/null
	 if [ $? -eq 0 ]; then
	 
		((comptador++))
	 fi

done
echo "$comptador"
exit 0

# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

for arg in $*
do
	echo "$arg" | egrep '.{4,}'	       

done
exit 0

