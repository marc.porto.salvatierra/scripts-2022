#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# suspes o aprovat
# prog nota
# -----------------------------------------


# 1) Validar arguments
ERR_NARG=1
ERR_RNOTA=2

if [ $# -ne 1 ];
then
	echo "Error: nº args incorrecte"
	echo "Usage: prog nota"
	exit $ERR_NARG
fi

# 2) Validar rang nota
nota=$1

if ! [ $nota -ge 0 -a $nota -le 10 ];
then
	echo "Error: rang de nota incorrecta"
	echo "La nota ha de ser 0-10"
	exit $ERR_RNOTA

fi

# 3) Programa

if [ $nota -lt 5 ];
then
	echo "La nota $nota és suspes"

else 
	echo "La nota $nota és aprovat"

fi
