#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Prog dir...
# -------------------------------------------------------

ERR_NARG=1
ERR_DIR=2

# 1) Validar arguments

if ! [ $# -gt 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 dir..."
        exit $ERR_NARG
fi

# 2) Si no es dir
for dir in $*
do
	if ! [ -d $dir ];
	then	
		echo "Error: directori no trobat"
		echo "L'argument rebut ha de ser un directori vàlid"
		
	else
		llista=$(ls $dir)
		for elem in $llista
		do
			if [ -L "$dir/$elem" ]; then
				echo "$elem es un link"
			elif [ -d "$dir/$elem" ]; then
				echo "$elem es un dir"
			elif [ -f "$dir/$elem" ]; then
				echo "$elem es un file"
			else
				echo "$elem es un altra cosa"
			
			fi
		done
	fi
done

exit 0
