#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
#
# Exemple processament arguments
#
# --------------------------------------------

# Variables per processar arguments
echo '$*: ' $* # llista args, no s'expandeix
echo '$@: ' $@ # llista args, s'expandeix en paraules separades
echo '$#: ' $# # num d'args totals
echo '$0: ' $0 # mostra l'arg 0 (mostrara el nom del file)
echo '$1: ' $1 # mostra l'arg 1
echo '$2: ' $2 # mostra l'arg 2
echo '$9: ' $9 # mostra l'arg 9
echo '$10: ' ${10} # obligatori posar claus quan son dos digits o mes
echo '$11: ' ${11} # " 
echo '$$: ' $$ # mostra el PID de l'execucio

nom="puig"
echo "${nom}deworld"
exit 0
