#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Exemple if : indica si es major d'edat
# prog edat
# -----------------------------------------

# 1) Validar arguments
if [ $# -ne 1 ];
then
	echo "Error: nº args incorrecte"
	echo "Usage: prog edat"
	exit 1
fi

# 2) Programa
edat=$1
if [ $edat -ge  18 ];
then
	echo "$edat, és major d'edat"
elif [ $edat -gt 0 -a $edat -lt 18 ];
then	
	echo "$edat, és menor d'edat"
	
fi
