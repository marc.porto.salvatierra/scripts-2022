#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Validar 2 arg
# primer arg es un file
# segon arg es un dir
# -------------------------------------------------------


ERR_NARG=1
ERR_FILE=2
ERR_DIR=3


# Validar 2 args
if [ $# -ne 2 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 file dir"
        exit $ERR_NARG
fi

# Si un file
file=$1
dir=$2

if ! [ -f $file ];then
	echo "Error: $file no valid"
	echo "Usage: $0 file dir"
        exit $ERR_FILE	

elif ! [ -d $dir ];then
	echo "Error: $dir no valid"
	echo "Usage: $0 file dir"
	exit $ERR_DIR

fi

cp $file $dir
exit 0
