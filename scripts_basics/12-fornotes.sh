#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Rep almenys una nota o mes
# Per cada nota diu si suspes aprovat notable o excelent
# -------------------------------------------------------

ERR_NARG=1
ERR_NOTA=2

# 1) Validar arguments

if ! [ $# -ge 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 nota ..."
        exit $ERR_NARG
fi


# 2) Validar rang

# 3) Sortir la nota
for nota in $*
do
	if [ $nota -lt 5 ]; then
		if ! [ $nota -ge 0 -a $nota -le 10 ]; then
			echo "Error: nota no valida"
			echo "Rang de nota: [0-10]"
			
		else
		       	echo "La nota $nota, es suspes "
		fi
       	
	elif [ $nota -lt 7 ]; then
		if ! [ $nota -ge 0 -a $nota -le 10 ]; then
			echo "Error: nota no valida"
			echo "Rang de nota: [0-10]"
			
		else
	       		echo "La nota $nota, es aprovat"
		fi
	
	elif [ $nota -lt 9 ]; then
		if ! [ $nota -ge 0 -a $nota -le 10 ]; then
			echo "Error: nota no valida"
			echo "Rang de nota: [0-10]"
			
		else
			echo "La nota $nota, es notable"
		fi
	
	else 
		if ! [ $nota -ge 0 -a $nota -le 10 ]; then
			echo "Error: nota no valida"
			echo "Rang de nota: [0-10]"
			
		else
			echo "La nota $nota, es excelent"
		fi

	fi		
done
exit 0


