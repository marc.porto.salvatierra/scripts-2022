#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Exemples de while
# -------------------------------------------------------

# Numerar entrada estandard linia linia i passar a majuscules
comptador=1
while read -r line
do
	echo "$comptador: $line" | tr '[:lower:]' '[:upper:]'
	((comptador++))
done
exit 0
# Processa l'entrada estandard fins token FI
read -r line
while [ $line != "FI" ]
do
	echo "$line"
	read -r line
done
exit 0
# Processa l'entrada estandar i mostrar numerada
comptador=1
while read -r line
do
	echo "$comptador: $line"
	((comptador++))
done
exit 0
# Processar entrada estandard linia a linia
while read -r line
do
	echo $line
done
exit 0
# Iterar args amb shift
while [ -n "$1" ]
do
	echo $1
	shift
done
exit 0
# Compta enrera amb argument rebut
comptador=$1
MIN=0
while [ $MIN -le $comptador ]
do
	echo "$comptador"
	((comptador--))
done
exit 0
# Mostrar un comptador deĺ 1 a MAX
MAX=10
comptador=1
while [ $comptador -le $MAX ]
do
	echo "$comptador"
	((comptador++))
done
exit 0
