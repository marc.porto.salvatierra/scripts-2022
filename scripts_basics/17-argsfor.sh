#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# prog -a pere -b marta joan -s -r -i ramon
# opcions: -a -b -c -i -s -r
# args: pere marta joan ramon -m
# -------------------------------------------------------     

ERR_NARG=1
# 1) Validar arguments

if ! [ $# -ge 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_NARG
fi

opcions=""
arguments=""
for elem in $*
do
	case $elem in
		"-a"|"-b"|"-c"|"-i"|"-s"|"-r")
			opcions="$opcions $elem"
			;;
		*)
			arguments="$arguments $elem"
			;;
	esac
done

echo "opcions: $opcions"
echo "arguments: $arguments"
exit 0
