#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Fer un ls de dir i numerar cada file
# -------------------------------------------------------

ERR_NARG=1
ERR_DIR=2

# 1) Validar arguments

if [ $# -ne 1 ];
then
        echo "Error: nº args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_NARG
fi

# 2) Si no es dir
dir=$1
if ! [ -d $dir ];
then	
	echo "Error: directori no trobat"
	echo "L'argument rebut ha de ser un directori vàlid"
	exit $ERR_DIR
fi

# 3) Llistar, numerant un a un els fitxers
comptador=1
llista=$(ls $dir)
for file in $llista
do
	echo "$comptador: $file"
	((comptador++))
done
exit 0
