#! /bin/bash
#
# @Marcporto ASIX-m01
# Febrer 2022
# Exercicis d'scripts basics
# -------------------------------------------------------

# 10) Prog que rep com a arg un numero indicatiu de linies a mstrar. Processa stdin linia a linia i mostra numerades un maxim de num linies
MAX=$1
comptador=1
while read -r line
do
	if [ $comptador -le $MAX ]; then	
		echo "$comptador: $line"
		((comptador++))
	fi	
	 
done
exit 0
# 9) Fer prog que rep users per stdin i verificarlos
while read -r line
do
	grep "^$line:" /etc/passwd > /dev/null
	if [ $? -eq 0 ]; then
		echo "Usuari $line correcte"
	else
			echo "Error: Usuari no valid" > /dev/stderr
	fi
done
exit 0
# 8) Fer prog que rep com args noms d'usuari, verificar si esxisteix en el sys
for user in $*
do
	grep "^$user:" /etc/passwd > /dev/null
	if [ $? -eq 0 ]; then
		echo "Usuari: $user correcte"
	else
		echo "Error: Usuari no valid"
	fi
done
exit 0
# 7) Processar linia a linia stdin, si la linia te més de 60 chars la mostra
while read -r line
do
	chars=$(echo "$line" | wc -m)
	if [ $chars -ge 60 ]; then
		echo "$line"
	fi
done
exit 0
# 6) Fer prog que rep com args els dies de setmana i mostra quants dies eran laborables i quants festius. Si l'arg no es un dia genera error per stderr
laborable=0
festiu=0
for dia in $*
do
	case $dia in
		"dilluns"|"dimarts"|"dimecres"|"dijous"|"dijous")
			((laborable++))
			;;
		"dissabte"|"diumenge")
			((festiu++))
			;;
		*)
			echo "Error: dia de la setmana no valid"
	esac
done
echo "Dies laborables: $laborable"
echo "Dies festius: $festiu"
exit 0
# 5) Mostrar linia a linia l'stdin, retallant els primers 50 chars
while read -r line
do
	echo "$line" | cut -c1-50
done
exit 0
# 4) Fer programa que rep com args num de mes (un o mes) i indica quants dies 
for mes in $*
do
	case $mes in
		"2")
			echo "El mes $mes te 28 dies"
			;;
		"4"|"6"|"9"|"11")
			echo "El mes $mes te 30 dies"
			;;
		*)
			echo "El mes $mes te 31 dies"
	esac	
done
exit 0
# 3) Fer comptador desde zero fins valor indicat per argument rebut
MAX=$1
comptador=1
while [ $comptador -le $MAX ]
do
	echo "$comptador"
	((comptador++))
done
exit 0
# 2) Mostrar arguments rebut linia a linia, numerant-los
comptador=1
for arg in $*
do
	echo "$comptador: $arg"
	((comptador++))
done
exit 0
# 1) Mostrar stdin numerant linia a linia
comptador=1
while read -r line
do
	echo "$comptador: $line"
	((comptador++))
done
exit 0
