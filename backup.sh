
# script dir nº retencions
#	backup /tmp/backup
#	purge
ERR_NOUSER=1
ERR_NOARGS=2
ERR_NODIR=3
ERR_NORET=4


dir=$1
ret=$2
# Validar que es root

ID=$(echo $UID)
if [ $ID -ne 0 ]; then
	echo "Error: usuari no valid"
	exit $ERR_NOUSER
fi

# Validar 2 args
if [ $# -ne 2 ]; then
	echo "Error: nº d'args incorrecte"
	echo "Usage: $0 arg1 arg2"
	exit $ERR_NOARGS
fi

# Validar existeix dir sino exit
if ! [ -d $dir ]; then 
	echo "Error: dir $dir incorrecte"
	exit $ERR_NODIR
fi

# Validar rtencions [2-10] sino exit
if [ $ret -lt 2 -o $ret -gt 10 ]; then
	echo "Error: nº de retencions no vàlid"
	echo "Usage: [2-10]"
	exit $ERR_NORET
fi	
# sino existeix backup crear
# genera nom bk

#tar czf /tmp/backup/m1-$(date +"%Y%m%d-%H%M").tgz $dir
#find /tmp/backup -name "m1-*" -type f -mmin +$ret -delete 
