#! /bin/bash
# @MarcPorto 
# Curs 2021-22
# ASIX M01-ISO
# Exemples funcions

# -----------------------------

function hola(){
	echo "hola"
	return 0
}


function dia(){
	date
	return 0
}


function suma(){
	echo "$(($1+$2))"
	return 0
}

function all(){
	hola
	echo "avui som: $(dia)"
	suma 6 9
}


function fsize(){
	login=$1
	grep "^$login:" /etc/passwd &> /dev/null
	if [ $? -ne 0 ]; then
		echo "Error: user $login inexistent"
		return 1
	fi
	home=$(egrep "^$login:" /etc/passwd | cut -d: -f6)
	du -sh $home 2> /dev/null
	return 0
}

function loginargs(){
	if [ $# -eq 0 ]; then
		echo "Error: nº d'arguments incorrecte"
		echo "Usage: $0 login..."
		return 1
	fi
	for login in $*
	do
		
		fsize $login
	
	done
	return 0
}

function loginfile(){
	if [ $# -eq 0 ]; then
	       	echo "Error: nº d'arguments incorrecte"
		echo "Usage: $0 file"
 		return 1		
	fi
	file=$1
	if ! [ -f "$file" ]; then
		echo "Error: fitxer inexistent"
		return 2
	fi
	while read -r login
	do
		fsize $login
	done < $file	
	return 0
}

function loginboth(){
	file=/dev/stdin
	if [ $# -eq 1 ]; then
		file=$1
	fi
	while read -r login
	do
		fsize $login
	done < $file
	
	return 0
}

function grepgid(){
	if [ $# -eq 0 ]; then
		echo "Error: nº d'arguments incorrecte"
		echo "Usage: prog arg"
		return 1
	fi
	gid=$1
	grep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
	if ! [ $? -eq 0 ]; then
		echo "Error: gid $gid inexistent"
		return 2
	fi
	# Mostrar llista de logins amb el mateix gid
	cut -d: -f1,4 /etc/passwd | grep ":$gid$"
}

function gidsize(){
	gid=$1

	llistalogin=$(grepgid $gid | cut -d: -f1)
	for login in $llistalogin
	do
		fsize $login
	done
}

function allgidsize(){
	llista_gid=$(cut -d: -f4 /etc/passwd | sort -gu)

	for gid in $llista_gid
	do
	#	llista_login=$(grepgid $gid | cut -d: -f1)
		echo "GID: $gid"
		gidsize $gid 2> /dev/null
	done
}

function filterGroup(){
	gid=0
	while [ $gid -ge 0 -a $gid -le 100 ]
	do
		grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd
		((gid++))	
	done
}

#Donat un fstype d'argument  llista el device i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.

function fstype(){
	fstype=$1
	egrep -v "^[[:blank:]]*$|^[[:blank:]]*#" /etc/fstab | tr -s [:blank:] ' ' | grep "^[^ ]* [^ ]* $fstype " cut -d' ' -f1,2 | sort -t' ' -k1

}

function allfstype(){
	llista=$(grep -v "^#" /etc/fstab | tr -s "[:blank:]" ' ' | cut -d' ' -f3 | sort -u)
	for fstype in $llista
	do
		grep -v "^#" /etc/fstab | tr -s "[:blank:]" ' ' | cut -d' ' -f3 | grep "^[^ ]* [^ ]* $fstype " | cut -d' ' -f3  
	done
	
}

function allfstypeif(){
	MAX=$1
	linia=$(grep -v "^#" /etc/fstab | tr -s "[:blank:]" ' ' | cut -d' ' -f3 | sort -u )
	for fstype in $llista
	do
		num=$(grep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | grep "^[^ ]* [^ ]* $fstype" | wc -l)
		if [$num -ge $MAX ]; then
			fstype $fstype
		fi
	done
	return 0



}
