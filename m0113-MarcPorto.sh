#! /bin/bash
#
# Juny 2022
#
# @MarcPorto
# Curs 2021-22
#
# Fer un script que rep com arguments un o mes logins i fa backup del seu home
#
# prog.sh pere...
#
#--------------------------------------------------

ERR_NOARGS=1
ERR_NOUSER=2

# Validar arguments
if [ $# -lt 1 ]; then
	echo "Error: nº d'arguments, $#, incorrecte"
	echo "Usage: prog.sh login..."
	exit $ERR_NOARGS

fi

# Validar usuari root
ID=$(echo $UID)
who=$(whoami)

if [ $ID -ne 0 ]; then
	echo "Error: usuari $who incorrecte"
	exit $ERR_NOUSER
fi

# Verificar si existeix dir /backup
cd /backup &> /dev/null
if [ $? -ne 0 ]; then
	echo "Directori /backup inexistent" > /dev/stderr
	echo "Creant diectori" > /dev/stderr
	mkdir /backup

fi


for user in $*
do
	home=$(grep "^$user" /etc/passwd | cut -d: -f6)
	grep "^$user:" /etc/passwd &> /dev/null
	if [ $? -ne 0 ]; then
		echo "Error: usuari no valid" > /dev/stderr

	elif [ -z $home ]; then
		echo "Error: home del user inexistent" > /dev/stderr
	
	else
		tar czf /backup/$user-$(date +"%Y%m%d-%H%M").tgz /home/$user 2> /dev/null
		echo "Creant backup de $user"
		find /backup/$user-* -type f -mtime +10 -delete
	fi

done
